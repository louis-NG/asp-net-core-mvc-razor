﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BookListRazor.Model;

namespace BookListRazor.Pages.View
{
    public class IndexModel : PageModel
    {
        private readonly BookListRazor.Model.BookDBContext _context;

        public IndexModel(BookListRazor.Model.BookDBContext context)
        {
            _context = context;
        }
        [TempData]
        public string Message { get; set; }
        public IList<Book> Book { get;set; }

        public async Task OnGetAsync()
        {
            Book = await _context.Books.ToListAsync();
        }
    }
}
