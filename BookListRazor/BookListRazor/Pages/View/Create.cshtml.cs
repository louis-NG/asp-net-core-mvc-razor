﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BookListRazor.Model;

namespace BookListRazor.Pages.View
{
    public class CreateModel : PageModel
    {
        private readonly BookListRazor.Model.BookDBContext _context;

        public CreateModel(BookListRazor.Model.BookDBContext context)
        {
            _context = context; 
        }

        public IActionResult OnGet()
        {
            return Page();
        }
        [TempData]
        public string Message { get; set; }
        [BindProperty]
        public Book Book { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Books.Add(Book);
            await _context.SaveChangesAsync();
            Message = "Book has been created succesfully.";
            return RedirectToPage("./Index");
        }
    }
}
